from Client import Client
from CommandLineInterface import CLI
from argparse import Namespace
from pyuv import Loop


def get_args():
    args = Namespace()
    args.host_ip = "0.0.0.0"
    args.host_port = 8864
    return args


def main():
    args = get_args()
    l = Loop.default_loop()
    intf = CLI()
    c = Client(intf, args.host_ip, args.host_port)
    intf.register_loop(l)
    c.register_loop(l)
    l.run()


if __name__ == '__main__':
    from sys import exit as sexit

    sexit(main())
