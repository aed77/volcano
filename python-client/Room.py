from typing import List, Tuple
from User import User


class Room:
    def __init__(self, users: List[User]):
        self.messages: List[Tuple[int, str, User]] = []
        self.users = users
