import volcano_pb2


class Encoder:

    @staticmethod
    def encode_auth(username: str, password: str) -> bytes:
        blob = volcano_pb2.Blob()
        blob.type = volcano_pb2.Blob.Command
        blob.cmd.command = volcano_pb2.Command.AUTH
        blob.cmd.arg1, blob.cmd.arg2 = username, password
        return blob.SerializeToString()

    @staticmethod
    def encode_add(username: str, password: str, email: str) -> bytes:
        blob = volcano_pb2.Blob()
        blob.type = volcano_pb2.Blob.Command
        blob.cmd.command = volcano_pb2.Command.ADD
        blob.cmd.arg1, blob.cmd.arg2, blob.cmd.arg3 = username, password, email
        return blob.SerializeToString()

    @staticmethod
    def encode_del(username: str) -> bytes:
        blob = volcano_pb2.Blob()
        blob.type = volcano_pb2.Blob.Command
        blob.cmd.command = volcano_pb2.Command.DEL
        blob.cmd.arg1 = username
        return blob.SerializeToString()

    @staticmethod
    def encode_list() -> bytes:
        blob = volcano_pb2.Blob()
        blob.type = volcano_pb2.Blob.Command
        blob.cmd.command = volcano_pb2.Command.LIST
        return blob.SerializeToString()

    @staticmethod
    def encode_request(user_to_request: str) -> bytes:
        blob = volcano_pb2.Blob()
        blob.type = volcano_pb2.Blob.Command
        blob.cmd.command = volcano_pb2.Command.REQUEST
        blob.cmd.arg1 = user_to_request
        return blob.SerializeToString()

    @staticmethod
    def encode_search(search_filter: str) -> bytes:
        blob = volcano_pb2.Blob()
        blob.type = volcano_pb2.Blob.Command
        blob.cmd.command = volcano_pb2.Command.SEARCH
        blob.cmd.arg1 = search_filter
        return blob.SerializeToString()

    @staticmethod
    def encode_conv(user_to_conv: str) -> bytes:
        blob = volcano_pb2.Blob()
        blob.type = volcano_pb2.Blob.Command
        blob.cmd.command = volcano_pb2.Command.CONV
        blob.cmd.arg1 = user_to_conv
        return blob.SerializeToString()

    @staticmethod
    def encode_quit() -> bytes:
        blob = volcano_pb2.Blob()
        blob.type = volcano_pb2.Blob.Command
        blob.cmd.command = volcano_pb2.Command.QUIT
        return blob.SerializeToString()

    @staticmethod
    def encode_message(timestamp: int, roomid: int, message: str, username: str) -> bytes:
        blob = volcano_pb2.Blob()
        blob.type = volcano_pb2.Blob.Msg
        blob.msg.timestamp = timestamp
        blob.msg.roomid = roomid
        blob.msg.message = message
        blob.msg.username = username
        return blob.SerializeToString()
