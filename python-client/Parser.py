from volcano_pb2 import Blob
from Commands import Command, Response, NewMessage


class BadData(Exception):
    """ Raised when a packet fails to parse """


class Parser:

    @staticmethod
    def parse_to_command(data: bytes) -> Command:
        b = Blob()
        b.ParseFromString(data)
        if b.type == Blob.Response:
            return Response(b.res.code, b.res.field)
        elif b.type == Blob.Msg:
            return NewMessage(b.msg.timestamp, b.msg.roomid, b.msg.message, b.msg.username)
        else:
            raise BadData()
