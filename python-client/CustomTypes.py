from enum import Enum


class Action(Enum):
    AddFriend = 1
    RemoveFriend = 2
    ListFriends = 3
    MessageRoom = 4
    LogOut = 5
    ListUsers = 6
    CheckRoomMessages = 7
    NewRoom = 8


class CommandResponses(Enum):
    AUTH = 0
    ADD = 1
    DEL = 2
    LIST = 3
    REQUEST = 4
    SEARCH = 5
    CONV = 6
    QUIT = 7
