from abc import ABC, abstractmethod
from CustomTypes import Action
from Room import Room
from FriendsList import FriendsList
from Friend import Friend
from User import User
from typing import List, Callable, Optional, Dict


class UserInterface(ABC):

    @abstractmethod
    def display_room(self, room: Room, callback: Callable[[], None]):
        raise NotImplementedError()

    @abstractmethod
    def prompt_for_action(self, callback: Callable[[Action], None]):
        raise NotImplementedError()

    @abstractmethod
    def display_friends_list(self, friends_list: FriendsList, callback: Callable[[], None]):
        raise NotImplementedError()

    @abstractmethod
    def prompt_for_user_pass(self, callback: Callable[[str, str], None]):
        raise NotImplementedError()

    @abstractmethod
    def prompt_for_friend_to_remove(self, friends_list: FriendsList, callback: Callable[[Optional[Friend]], None]):
        raise NotImplementedError()

    @abstractmethod
    def prompt_for_person_to_add(self, users: List[User], callback: Callable[[Optional[User]], None]):
        raise NotImplementedError()

    @abstractmethod
    def prompt_for_create_room(self, users: FriendsList, callback: Callable[[Optional[Friend]], None]):
        raise NotImplementedError()

    @abstractmethod
    def prompt_to_message_room(self, room: Room, callback: Callable[[str], None]):
        raise NotImplementedError()

    @abstractmethod
    def display_list_of_users(self, users: List[User], callback: Callable[[], None]):
        raise NotImplementedError()

    @abstractmethod
    def log_error(self, message: str):
        raise NotImplementedError()

    @abstractmethod
    def display_message(self, message: str):
        raise NotImplementedError()

    @abstractmethod
    def prompt_for_room_to_message(self, rooms: Dict[int, Room], callback: Callable[[int, Room], None]):
        raise NotImplementedError()

    @abstractmethod
    def prompt_for_room_to_show(self, rooms: Dict[int, Room], callback: Callable[[int, Room], None]):
        raise NotImplementedError()