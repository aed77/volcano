from abc import abstractmethod, ABC
from typing import List
from User import User

class Command(ABC):

    @abstractmethod
    def __call__(self, user):
        raise NotImplementedError()


class NewMessage(Command):
    def __init__(self, timestamp: int, roomid: int, msg: str, username: str):
        self._timestamp = timestamp
        self._roomid = roomid
        self._msg = msg
        self._username = username

    def __call__(self, user):
        user.on_message(self._timestamp, self._roomid, self._msg, User(self._username))


class Response(Command):
    def __init__(self, code: bool, fields: List[str]):
        self._code = code
        self._fields = fields

    def __call__(self, user):
        user.on_response(self._code, self._fields)
