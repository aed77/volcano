from pyuv import Loop, TCP, Timer
from UserInterface import UserInterface
from typing import Optional, List, Dict, Callable
from Encoder import Encoder
from Parser import Parser
from CustomTypes import CommandResponses, Action
from FriendsList import FriendsList
from Room import Room
from User import User
from collections import defaultdict
from Friend import Friend
from time import time


class Client:
    def __init__(self, interface: UserInterface, server_ip: str, server_port: int, parser: Parser = Parser(),
                 encoder: Encoder = Encoder()):
        self._ui = interface
        self._tcp_handle: TCP = None
        self._connection_string = (server_ip, server_port)
        self._authenticated = False
        self._parser = parser
        self._encoder = encoder
        self._friends_list = FriendsList([])
        self._on_next_response = lambda x, y: None
        self._rooms: Dict[int, Room] = {}
        self._me: User = None

    def register_loop(self, loop: Loop):
        self._tcp_handle = TCP(loop)
        self._tcp_handle.connect(self._connection_string, self.on_connection_start)

    def on_data(self, socket: TCP, data: bytes, error: Optional[int]):
        if not error:
            cmd = self._parser.parse_to_command(data)
            cmd(self)

    def on_message(self, timestamp: int, roomid: int, message: str, user: User):
        if roomid not in self._rooms:
            self._rooms[roomid] = Room([self._me, user])
        self._rooms[roomid].messages.append((timestamp, message, user))

    def on_response(self, success: bool, data: List[str]):
        self._on_next_response(success, data)

    def on_connection_start(self, socket: TCP, error: Optional[int]):
        socket.start_read(self.on_data)

        # Start the user interaction since we opened the socket
        def on_user_pass(username: str, password: str):
            def on_response(success: bool, data: List[str]):
                if success:
                    self._me = User(username)
                    self.refresh_friends_list(self.prompt_for_action)
                else:
                    self._ui.prompt_for_user_pass(on_user_pass)

            self._on_next_response = on_response
            socket.write(self._encoder.encode_auth(username, password))

        self._ui.prompt_for_user_pass(on_user_pass)

    def prompt_for_action(self):
        self._ui.prompt_for_action(self.on_user_action)

    def refresh_friends_list(self, callback: Callable[[], None]):

        def on_response(success: bool, data: List[str]):
            if success:
                self._friends_list = FriendsList([Friend(x) for x in data])
                callback()
            else:
                self._ui.log_error("Got bad response refreshing friends list, trying again")

                def after_timer(timer_handle: Timer):
                    timer_handle.stop()
                    self.refresh_friends_list(callback)

                Timer(self._tcp_handle.loop).start(after_timer, 2, 0)

        self._on_next_response = on_response
        self._tcp_handle.write(self._encoder.encode_list())

    def on_user_action(self, action: Action):
        if action == Action.AddFriend:
            # First request the list of users
            def on_all_users(success: bool, data: List[str]):
                # Then ask the user which one they want to add
                if success:
                    def on_user_selected(user: Optional[User]):
                        def on_request_response(success: bool, data: List[str]):
                            if success:
                                self._ui.display_message("Requested or added user!")
                                self.refresh_friends_list(self.prompt_for_action)
                            else:
                                self._ui.log_error("Failed to add user")
                                self.prompt_for_action()
                        if user:
                            self._on_next_response = on_request_response
                            self._tcp_handle.write(self._encoder.encode_request(user.username))
                        else:
                            self.prompt_for_action()

                    self._ui.prompt_for_person_to_add([User(x) for x in data], on_user_selected)
                else:
                    self._ui.log_error("Failed to get list of users")
                    self.prompt_for_action()

            self._on_next_response = on_all_users
            self._tcp_handle.write(self._encoder.encode_search(""))
        elif action == Action.RemoveFriend:
            def after_refresh():
                def got_friend_to_remove(friend: Optional[Friend]):
                    def on_response(success: bool, data: List[str]):
                        if success:
                            self._ui.display_message("Removed friend")
                            self.refresh_friends_list(self.prompt_for_action)
                        else:
                            self._ui.log_error("Failed to remove friend")
                            self.prompt_for_action()

                    self._on_next_response = on_response
                    if friend:
                        self._tcp_handle.write(self._encoder.encode_del(friend.username))
                    else:
                        self.prompt_for_action()

                self._ui.prompt_for_friend_to_remove(self._friends_list, got_friend_to_remove)

            self.refresh_friends_list(after_refresh)

        elif action == Action.ListFriends:
            def after_refresh_friends_list():
                self._ui.display_friends_list(self._friends_list, self.prompt_for_action)

            self.refresh_friends_list(after_refresh_friends_list)
        elif action == Action.MessageRoom:
            def on_room_selected(room_id: int, room: Room):
                def on_message_typed(message: str):
                    def on_response(success: bool, data: List[str]):
                        if success:
                            self._ui.display_room(room, self.prompt_for_action)
                        else:
                            self._ui.log_error("Failed to message room")
                            self.prompt_for_action()

                    self._on_next_response = on_response
                    self._tcp_handle.write(
                        self._encoder.encode_message(timestamp=int(time()), roomid=room_id, message=message, username=self._me.username))

                self._ui.prompt_to_message_room(room, on_message_typed)

            self._ui.prompt_for_room_to_message(self._rooms, on_room_selected)
        elif action == Action.LogOut:
            def on_response(success: bool, data: List[str]):
                self._tcp_handle.loop.stop()

            self._on_next_response = on_response
            self._tcp_handle.write(self._encoder.encode_quit())
        elif action == Action.ListUsers:
            def on_response(success: bool, data: List[str]):
                if success:
                    self._ui.display_list_of_users([User(x) for x in data], self.prompt_for_action)
                else:
                    self._ui.log_error("Failed to get list of users")
                    self.prompt_for_action()

            self._on_next_response = on_response
            self._tcp_handle.write(self._encoder.encode_search(""))
        elif action == Action.CheckRoomMessages:
            def on_get_room(room_id: int, room: Room):
                self._ui.display_room(room, self.prompt_for_action)

            self._ui.prompt_for_room_to_show(self._rooms, on_get_room)

        elif action == Action.NewRoom:
            def on_got_user(friend: Friend):
                def on_response(success: bool, data: List[str]):
                    if success:
                        self._ui.display_message("Created room")
                        self._rooms[int(data[0])] = Room([self._me, friend])
                        self.prompt_for_action()
                    else:
                        self._ui.log_error("Failed to create room")
                if friend:
                    self._on_next_response = on_response
                    self._tcp_handle.write(self._encoder.encode_conv(friend.username))
                else:
                    self.prompt_for_action()

            self._ui.prompt_for_create_room(self._friends_list, on_got_user)
        else:
            self._ui.log_error("Bad input")