from typing import List, Optional, Callable, Dict

from CustomTypes import Action
from Friend import Friend
from FriendsList import FriendsList
from Room import Room
from User import User
from UserInterface import UserInterface
from pyuv import Loop, TTY
from sys import stdin, stdout, stderr


class CLI(UserInterface):
    def __init__(self):
        self._input_handle: TTY = None
        self._output_handle: TTY = None

    def register_loop(self, loop: Loop):
        self._input_handle = TTY(loop, stdin.fileno(), True)
        self._output_handle = TTY(loop, stdout.fileno(), False)

    def display_room(self, room: Room, callback: Callable):
        if room.messages:
            self._output_handle.write(b"Messages: \n")
            for message in room.messages:
                self._output_handle.write(
                    "[{timestamp}] {user}: {message}\n".format(timestamp=message[0], user=message[2].username,
                                                               message=message[1]).encode())
        else:
            self._output_handle.write(b"No messages in this room \n")
        callback()

    def prompt_for_action(self, callback: Callable[[Action], None]):
        def on_data(tty: TTY, data: bytes, error: Optional[int]):
            # Strip off newline
            data = data[:-1]
            self._input_handle.stop_read()
            int_to_action = {
                1: Action.AddFriend,
                2: Action.RemoveFriend,
                3: Action.ListFriends,
                4: Action.MessageRoom,
                5: Action.LogOut,
                6: Action.ListUsers,
                7: Action.CheckRoomMessages,
                8: Action.NewRoom
            }
            try:
                action = int_to_action[int(data)]
            except KeyError:
                self.prompt_for_action(callback)
            except TypeError:
                self.prompt_for_action(callback)
            except ValueError:
                self.prompt_for_action(callback)
            else:
                callback(action)

        self._output_handle.write(b"""
Options:
1) Add Friend
2) Remove Friend
3) List Friends
4) Message Room
5) Log Out
6) List Users
7) List Room Messages
8) New Room\n""")
        self._output_handle.write(b"Please input an action: ")
        self._input_handle.start_read(on_data)

    def display_friends_list(self, friends_list: FriendsList, callback: Callable[[], None]):
        self._output_handle.write(b"Friends:\n")
        for user in friends_list:
            self._output_handle.write("\t{},\n".format(user.username).encode())
        callback()

    def prompt_for_user_pass(self, callback: Callable[[str, str], None]):
        self._output_handle.write(b"Please input your Username: ")

        def on_data(tty: TTY, data: bytes, error: Optional[int]):
            # Strip off newline
            data = data[:-1]
            self._input_handle.stop_read()
            username = data.decode()
            self._output_handle.write(b"Please input your Password: ")

            def on_more_data(tty: TTY, more_data: bytes, error: Optional[int]):
                # Strip off newline
                more_data = more_data[:-1]
                self._input_handle.stop_read()
                password = more_data.decode()
                callback(username, password)

            self._input_handle.start_read(on_more_data)

        self._input_handle.start_read(on_data)

    def prompt_for_friend_to_remove(self, friends_list: FriendsList, callback: Callable[[Optional[Friend]], None]):
        def after_friends_done():
            def on_data(tty: TTY, data: bytes, error: Optional[int]):
                # Strip off newline
                data = data[:-1]
                self._input_handle.stop_read()
                if User(data.decode()) in friends_list:
                    callback(next(x for x in friends_list if x == User(data.decode())))
                else:
                    callback(None)

            self._output_handle.write(b"Please pick a user to remove: ")
            self._input_handle.start_read(on_data)

        self.display_friends_list(friends_list, after_friends_done)

    def prompt_for_person_to_add(self, users: List[User], callback: Callable[[Optional[User]], None]):
        def after_displayed_users():
            def on_data(tty: TTY, data: bytes, error: Optional[int]):
                # Strip off newline
                data = data[:-1]
                self._input_handle.stop_read()
                if User(data.decode()) in users:
                    callback(next(x for x in users if x == User(data.decode())))
                else:
                    callback(None)

            self._output_handle.write(b"Please pick a user to request: ")
            self._input_handle.start_read(on_data)

        self.display_list_of_users(users, after_displayed_users)

    def prompt_for_create_room(self, friends_list: FriendsList, callback: Callable[[Optional[Friend]], None]):
        def after_friends_done():
            def on_data(tty: TTY, data: bytes, error: Optional[int]):
                # Strip off newline
                data = data[:-1]
                self._input_handle.stop_read()
                if User(data.decode()) in friends_list:
                    callback(next(x for x in friends_list if x == User(data.decode())))
                else:
                    callback(None)

            self._output_handle.write(b"Please pick a user to start a chat with: ")
            self._input_handle.start_read(on_data)

        self.display_friends_list(friends_list, after_friends_done)

    def prompt_to_message_room(self, room: Room, callback: Callable[[str], None]):
        def after_room_displayed():
            def on_data(tty: TTY, data: bytes, error: Optional[int]):
                # Strip off newline
                data = data[:-1]
                self._input_handle.stop_read()
                callback(data.decode())

            self._output_handle.write(b"Message: ")
            self._input_handle.start_read(on_data)

        self.display_room(room, after_room_displayed)

    def display_list_of_users(self, users: List[User], callback: Callable[[], None]):
        self._output_handle.write(b"Users:\n")
        for user in users:
            self._output_handle.write("\t{}\n".format(user.username).encode())
        callback()

    def log_error(self, message: str):
        print("Error: {}\n".format(message), file=stderr)

    def display_message(self, message: str):
        self._output_handle.write((message + "\n").encode())

    def prompt_for_room_to_message(self, rooms: Dict[int, Room], callback: Callable[[int, Room], None]):
        self._output_handle.write(b"Rooms:\n")
        for room_id, room in rooms.items():
            self._output_handle.write("\tid: {}\n\tUsers who sent a message:\n".format(room_id).encode())
            for user in room.users:
                self._output_handle.write("\t\t{}\n".format(user.username).encode())

        def on_data(tty: TTY, data: bytes, error: Optional[int]):
            # Strip off newline
            data = data[:-1]
            self._input_handle.stop_read()
            try:
                room = rooms[int(data)]
            except KeyError:
                self.log_error("Invalid room ID")
                self.prompt_for_room_to_message(rooms, callback)
            except ValueError:
                self.log_error("Invalid room ID")
                self.prompt_for_room_to_message(rooms, callback)
            else:
                callback(int(data), room)

        self._output_handle.write(b"Enter id for room to message: ")
        self._input_handle.start_read(on_data)

    def prompt_for_room_to_show(self, rooms: Dict[int, Room], callback: Callable[[int, Room], None]):
        self._output_handle.write(b"Rooms:\n")
        for room_id, room in rooms.items():
            self._output_handle.write("\tid: {}\n\tUsers who sent a message:\n".format(room_id).encode())
            for user in room.users:
                self._output_handle.write("\t\t{}\n".format(user.username).encode())

        def on_data(tty: TTY, data: bytes, error: Optional[int]):
            # Strip off newline
            data = data[:-1]
            self._input_handle.stop_read()
            try:
                room = rooms[int(data)]
            except KeyError:
                self.log_error("Invalid room ID")
                self.prompt_for_room_to_show(rooms, callback)
            except ValueError:
                self.log_error("Invalid room ID")
                self.prompt_for_room_to_show(rooms, callback)
            else:
                callback(int(data), room)

        self._output_handle.write(b"Enter id for room to show: ")
        self._input_handle.start_read(on_data)
