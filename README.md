# Volcano!


## Requirements
---
* Java GUI Client: Java 10.0.1 + Swing + protobuf bindings
* Python Command line Client: python3 + protobuf bindings
* Server: C++17

## Compiling
---
* Java GUI Client: Use intellij to build
* Python command line client: run \_\_main\_\_.py
* Server
```
cd server
cmake .
make
```

## Installation
---
No installation process

### Database
An example database is provided as DB14 within the volcano source code.

Edit server/main.cpp to reflect the correct path to this database.

## Execution
---
* Java GUI Client - Execute within intellij or a compiles binary
* Python Commandline client
```
python3 __main__.py
```
* Server
```
./server
```

![alt text](http://s.newsweek.com/sites/www.newsweek.com/files/styles/lg/public/2016/03/03/topshot-ecuador-volcano.jpg "Logo")
