package com.volcano.interfaces;

import com.volcano.common.UpdateType;

import java.util.List;

public interface IObservable {
    void update(UpdateType dataType, List<String> data);
}
