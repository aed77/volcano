package com.volcano.interfaces;

import com.volcano.common.UpdateType;

import java.util.List;

public interface ISubject {
    void Register(IObservable view);
    void UnRegister(IObservable view);
    void Notify(UpdateType dataType, List<String> data);
}
