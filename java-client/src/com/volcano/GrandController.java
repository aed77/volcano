package com.volcano;

import com.volcano.controllers.BaseController;
import com.volcano.controllers.MainViewController;
import com.volcano.controllers.LoginController;
import com.volcano.controllers.RegisterController;
import com.volcano.models.BaseModel;
import com.volcano.models.FriendMessageModel;
import com.volcano.views.BaseView;
import com.volcano.views.LoginView;
import com.volcano.views.MainView;
import com.volcano.views.RegisterView;

public class GrandController {
    public enum View { NONE, LOGIN, REGISTER, MAIN }
    private BaseView currentV = null;
    private BaseController currentC = null;
    private BaseModel currentM = null;

    GrandController() { pageNav(View.LOGIN); }

    private void pageNav(View state) {
        switch(state) {
            case LOGIN:
                if (currentV != null)
                    currentV.close();
                currentV = new LoginView("Login", 500, 500);
                currentC = new LoginController((LoginView)currentV, this::pageNav);
                currentV.setVisible(true);
                return;
            case REGISTER:
                if (currentV != null)
                    currentV.close();
                currentV = new RegisterView("newUser", 500, 600);
                currentC = new RegisterController((RegisterView)currentV, this::pageNav);
                currentV.setVisible(true);
                return;
            case MAIN:
                if (currentV != null)
                    currentV.close();
                currentV = new MainView("Volcano", 750, 500);
                currentM = new FriendMessageModel();
                currentM.Register(currentV);
                currentC = new MainViewController((MainView) currentV, (FriendMessageModel) currentM);
                currentV.setVisible(true);
        }
    }
}