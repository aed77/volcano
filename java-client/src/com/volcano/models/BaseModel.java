package com.volcano.models;

import com.volcano.interfaces.IObservable;
import com.volcano.interfaces.ISubject;

import java.util.ArrayList;

public abstract class BaseModel implements ISubject {
    protected ArrayList<IObservable> subscribers;

    public BaseModel(){ subscribers = new ArrayList<>(); }

    @Override
    public void Register(IObservable view) { subscribers.add(view); }

    @Override
    public void UnRegister(IObservable view) { subscribers.remove(view); }
}
