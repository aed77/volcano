package com.volcano.models;

import java.util.ArrayList;
import java.util.List;

import com.volcano.common.UpdateType;
import com.volcano.connection.ServerConnection;

public class FriendMessageModel extends BaseModel {
    private ServerConnection conn = ServerConnection.getInstance();
    private ArrayList<Integer> roomIds;

    public FriendMessageModel(){
        super();
        roomIds = new ArrayList<>();
    }

    public void updateFriendList() {
        var res = conn.listFriends();
        Notify(UpdateType.FRIEND, res.Data);
    }

    public void updateSearchResults(String username) {
        var res = conn.searchUsers(username);
        Notify(UpdateType.SEARCH, res.Data);
    }

    public void requestFriend(String friend) {
        var res = conn.requestFriend(friend);
        if (res.Code)
            updateFriendList();
    }

    public void checkNewMessages() {
        var res = conn.readMessage();
        if (res != null)
            Notify(UpdateType.MESSAGE, List.of(""+res.RoomID, ""+res.Time, res.Sender, res.Text));
    }

    public void sendNewMessage(String msg) {
        System.out.println("Sending Message: " + msg);
        conn.sendMessage(msg, roomIds.get(0));
    }

    public void startChat(String username) {
        var res = conn.startChat(username);
        System.out.println(res.Data.get(0));
        roomIds.add(Integer.parseInt(res.Data.get(0)));
    }

    @Override
    public void Notify(UpdateType dataType, List<String> data) {
        for (var i : this.subscribers)
            i.update(dataType, data);
    }
}
