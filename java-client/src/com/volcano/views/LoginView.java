package com.volcano.views;

import com.volcano.common.UpdateType;

import javax.swing.*;
import java.awt.*;

import java.util.Map;
import java.util.List;
import java.util.function.Consumer;

public class LoginView extends BaseView {
    public enum Callbacks {
        LOGIN, REGISTER
    }

    private JTextField userField;
    private JPasswordField passField;
    private JButton loginBtn;
    private JButton regBtn;
    private JLabel errorLabel;

    public LoginView(String title, int width, int height) {
        super(title, width, height);

        var userLabel = new JLabel("username: ");
        userField = new JTextField();
        userField.setPreferredSize(new Dimension(100,25));

        var passLabel = new JLabel("password: ");
        passField = new JPasswordField();
        passField.setPreferredSize(new Dimension(100,25));

        errorLabel = new JLabel();
        errorLabel.setForeground(Color.RED);
        errorLabel.setVisible(false);

        loginBtn = new JButton("Login");
        regBtn = new JButton("Register");

        var panel = new JPanel(new GridBagLayout());
        panel.add(userLabel, new GridBagConstraints(0,0,1,1,0,0,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0,0,0,0),0,0));
        panel.add(userField, new GridBagConstraints(1,0,1,1,0,0,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0,0,0,0),0,0));
        panel.add(passLabel, new GridBagConstraints(0,1,1,1,0,0,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0,0,0,0),0,0));
        panel.add(passField, new GridBagConstraints(1,1,1,1,0,0,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0,0,0,0),0,0));
        panel.add(errorLabel, new GridBagConstraints(1,3,1,1,0,0,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0,0,0,0),0,0));
        panel.add(loginBtn, new GridBagConstraints(0,4,1,1,0,0,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0,0,0,0),0,0));
        panel.add(regBtn, new GridBagConstraints(1,4,1,1,0,0,GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0,0,0,0),0,0));

        super.setMainFrame(panel);
    }

    public void addCallbacks(Map<Callbacks, Consumer<Map>> callbacks) {
        if (callbacks.containsKey(Callbacks.LOGIN))
            loginBtn.addActionListener(e -> callbacks.get(Callbacks.LOGIN).accept(Map.of("Username", userField.getText(), "Password", new String (passField.getPassword()))));
        if (callbacks.containsKey(Callbacks.REGISTER))
            regBtn.addActionListener(e -> callbacks.get(Callbacks.REGISTER).accept(Map.of()));
    }

    @Override
    public void update(UpdateType dataType, List<String> data) {
        System.out.println("Update");
    }

    public void displayError(String err) {
        errorLabel.setText(err);
        errorLabel.setVisible(true);
    }
}
