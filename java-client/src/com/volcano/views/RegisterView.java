package com.volcano.views;

import com.volcano.common.UpdateType;

import javax.swing.*;
import java.awt.*;

import java.util.Map;
import java.util.List;
import java.util.function.Consumer;

public class RegisterView extends BaseView {
    public enum Callbacks {
        REGISTER, BACK
    }

    private JTextField userField;
    private JTextField userEmail;
    private JPasswordField passField;
    private JPasswordField passField2;
    private JButton registerBtn2;
    private JButton backBtn;
    private JLabel errorLabel;

    public RegisterView(String title, int width, int height) {
        super(title, width, height);

        var userLabel = new JLabel("username: ");
        userField = new JTextField();
        userField.setPreferredSize(new Dimension(100, 25));

        var userEmailLabel = new JLabel("email: ");
        userEmail = new JTextField();
        userEmail.setPreferredSize(new Dimension(100, 25));

        var passLabel = new JLabel("password: ");
        passField = new JPasswordField();
        passField.setPreferredSize(new Dimension(100, 25));

        var passLabel2 = new JLabel("password 2: ");
        passField2 = new JPasswordField();
        passField2.setPreferredSize(new Dimension(100, 25));

        errorLabel = new JLabel();
        errorLabel.setForeground(Color.RED);
        errorLabel.setVisible(false);

        registerBtn2 = new JButton("Register");
        backBtn = new JButton("Back");

        var panel = new JPanel(new GridBagLayout());
        panel.add(userLabel, new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        panel.add(userField, new GridBagConstraints(1, 0, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        panel.add(userEmailLabel, new GridBagConstraints(0, 1, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        panel.add(userEmail, new GridBagConstraints(1, 1, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        panel.add(passLabel, new GridBagConstraints(0, 2, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        panel.add(passField, new GridBagConstraints(1, 2, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        panel.add(passLabel2, new GridBagConstraints(0, 3, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        panel.add(passField2, new GridBagConstraints(1, 3, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        panel.add(errorLabel, new GridBagConstraints(1, 5, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        panel.add(registerBtn2, new GridBagConstraints(0, 6, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        panel.add(backBtn, new GridBagConstraints(1, 6, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        super.setMainFrame(panel);
    }

    public void addCallbacks(Map<Callbacks, Consumer<Map>> callbacks) {
        if (callbacks.containsKey(Callbacks.REGISTER)) {
            registerBtn2.addActionListener(e -> callbacks.get(Callbacks.REGISTER).accept(Map.of("Username", userField.getText(),"Email", userEmail.getText(), "Password", new String(passField.getPassword()))));
        }
        if (callbacks.containsKey(Callbacks.BACK)) {
            backBtn.addActionListener(e -> callbacks.get(Callbacks.BACK).accept(Map.of()));
        }
    }

    @Override
    public void update(UpdateType dataType, List<String> data) {
        System.out.println("Update");
    }

    public void displayError(String err) {
        errorLabel.setText(err);
        errorLabel.setVisible(true);
    }
}