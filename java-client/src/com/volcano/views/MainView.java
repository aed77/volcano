package com.volcano.views;

import com.volcano.common.UpdateType;

import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.function.Consumer;

public class MainView extends BaseView {
    public enum Callbacks {
        SEARCH, CHAT, MESSAGE, OUT, REQUEST
    }

    private JTabbedPane tabs;
    private JPanel friendPane;
    private Box searchResults;
    private Box chatWindow;
    private int chats = 0;
    private JTextField search;

    private Map<Callbacks, Consumer<Map>> callbacks;

    public MainView(String title, int width, int height) {
        super(title, width, height);

        var allView = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, friendPane(), searchPane());
        allView.setDividerLocation(200);

        tabs = new JTabbedPane();
        tabs.add("Friends", allView);

        super.setMainFrame(tabs);
    }

    private JPanel friendPane() {
        friendPane = new JPanel(new BorderLayout());
        var title = new JLabel("Active Friends", JLabel.CENTER);
        title.setFont(title.getFont().deriveFont(26f));
        friendPane.add(title, BorderLayout.NORTH);
        return friendPane;
    }

    private JPanel searchPane() {
        JPanel searchPane = new JPanel(new BorderLayout());
        searchResults = new Box(BoxLayout.Y_AXIS);
        var title = new JLabel("Find Friends", JLabel.CENTER);
        title.setFont(title.getFont().deriveFont(26f));
        var searchCombo = new Box(BoxLayout.X_AXIS);
        searchCombo.add(new JLabel("Search:  "));

        search = new JTextField();
        searchCombo.add(search);

        searchPane.add(title, BorderLayout.NORTH);
        searchPane.add(searchResults, BorderLayout.CENTER);
        searchPane.add(searchCombo, BorderLayout.SOUTH);
        return searchPane;
    }

    private JPanel chatTab() {
        var layout = new JPanel(new BorderLayout());
        var chatArea = new Box(BoxLayout.Y_AXIS);
        var chatBox = new JTextField();
        chatBox.addActionListener(e -> {
            callbacks.get(Callbacks.OUT).accept(Map.of("Field", chatBox.getText()));
            chatBox.setText("");
        });
        var chatBtn = new JButton();

        var chatCombo = new Box(BoxLayout.X_AXIS);
        chatCombo.add(chatBox, chatBtn);

        layout.add(chatArea, BorderLayout.CENTER);
        layout.add(chatCombo, BorderLayout.SOUTH);

        chatWindow = chatArea;

        return layout;
    }

    public void addCallbacks(Map<Callbacks, Consumer<Map>> callbacks) {
        this.callbacks = callbacks;
        search.addActionListener(e -> {
            callbacks.get(Callbacks.SEARCH).accept(Map.of("Field", search.getText()));
            search.setText("");
        });
        var timer = new Timer(1000, e -> {
            callbacks.get(Callbacks.MESSAGE).accept(new HashMap());
        });
        timer.start();
    }

    private void updateFriendList(List<String> data) {
        var friendList = new JPanel(new GridBagLayout());
        for (int i = 0; i < data.size(); ++i) {
            var userBtn = new JButton(data.get(i));
            userBtn.setBackground(Color.WHITE);
            userBtn.addActionListener(e -> {
                callbacks.get(Callbacks.CHAT).accept(Map.of("Field", userBtn.getText()));
                tabs.add(userBtn.getText(), chatTab());
                chats++;
                tabs.setSelectedIndex(chats);
            });
            friendList.add(userBtn, new GridBagConstraints(0,i,1,1,1,0,GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0),0,0));
        }

        var center = ((BorderLayout)(friendPane.getLayout())).getLayoutComponent(BorderLayout.CENTER);
        if (center != null)
            friendPane.remove(center);
        friendPane.add(friendList, BorderLayout.CENTER);
        friendPane.revalidate();
        friendPane.repaint();
    }
    private void updateSearchResults(List<String> data) {
        searchResults.removeAll();
        for (var f : data){
            var userBtn = new JButton(f);
            userBtn.setBackground(Color.WHITE);
            userBtn.addActionListener(e -> {
                callbacks.get(Callbacks.REQUEST).accept(Map.of("Field", userBtn.getText()));
            });
            searchResults.add(userBtn);
        }
        searchResults.revalidate();
        searchResults.repaint();
    }

    private void drawMessage(String Time, String Text, String user) {
        var container = Box.createHorizontalBox();

        var msg = new JLabel(Text  + "@" + new Date(Long.parseLong(Time) * 1000L));
        msg.setMinimumSize(new Dimension(20, 80));


        if (user.equals("")) {
            container.add(msg);
            container.add(Box.createHorizontalGlue());
            msg.setForeground(Color.BLUE);
            msg.setBorder(BorderFactory.createLineBorder(Color.BLUE));
        } else {
            container.add(Box.createHorizontalGlue());
            container.add(msg);
            msg.setForeground(Color.RED);
            msg.setBorder(BorderFactory.createLineBorder(Color.RED));
        }

        chatWindow.add(container);
        chatWindow.revalidate();
        chatWindow.repaint();
    }

    @Override
    public void update(UpdateType dataType, List<String> data) {
        switch (dataType) {
            case FRIEND:
                updateFriendList(data);
                break;
            case SEARCH:
                updateSearchResults(data);
            case MESSAGE:
                System.out.println("New MESSAGE IN VIEW!");
                try {
                    drawMessage(data.get(1), data.get(3), data.get(2));
                } catch (Exception e) {}
        }
    }
}
