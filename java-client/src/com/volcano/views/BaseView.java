package com.volcano.views;

import com.volcano.interfaces.IObservable;

import javax.swing.*;

public abstract class BaseView implements IObservable {

    private JFrame mainFrame;
    BaseView(String viewTitle, int width, int height) {
        mainFrame = new JFrame(viewTitle);
        mainFrame.setSize(width, height);
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    protected void setMainFrame(JComponent panel){
        mainFrame.setContentPane(panel);
    }

    public void setVisible(boolean bool) {
        mainFrame.setVisible(bool);
    }

    public void close() {
        mainFrame.setVisible(false);
        mainFrame.dispose();
    }
}
