package com.volcano.common;

public class Message {
    public final int Time;
    public final int RoomID;
    public final String Text;
    public final String Sender;

    public Message(int Time, int RoomID, String Text, String Sender) {
        this.Time = Time;
        this.RoomID = RoomID;
        this.Text = Text;
        this.Sender = Sender;
    }
}
