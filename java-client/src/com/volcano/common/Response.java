package com.volcano.common;

import java.util.List;

public class Response {
    public final boolean Code;
    public final List<String> Data;

    public Response(boolean Code, List<String> Data) {
        this.Code = Code;
        this.Data = Data;
    }
}
