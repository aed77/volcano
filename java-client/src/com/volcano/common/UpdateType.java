package com.volcano.common;

public enum UpdateType {
    FRIEND, SEARCH, MESSAGE
}
