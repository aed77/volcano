package com.volcano.connection;

import com.volcano.common.Message;
import com.volcano.connection.ProtoMessages.Blob;
import com.volcano.connection.ProtoMessages.Command;
import com.volcano.common.Response;

public class ServerConnection {
    private ProtoLink link;
    private String currentUser;

    private static ServerConnection instance = null;
    public static ServerConnection getInstance() {
        if (instance == null)
            instance = new ServerConnection();
        return instance;
    }


    private ServerConnection() { link = new ProtoLink("10.250.101.65", 8864); }

    private Blob buildCommand(Command.Cmd code, String arg1, String arg2, String arg3) {
        var blob = Blob.newBuilder();
        var cmd = Command.newBuilder();

        cmd.setCommand(code);
        if (!arg1.equals(""))
            cmd.setArg1(arg1);
        if (!arg2.equals(""))
            cmd.setArg2(arg2);
        if (!arg3.equals(""))
            cmd.setArg3(arg3);

        blob.setType(Blob.Type.Command);
        blob.setCmd(cmd);

        return blob.build();
    }

    private Response sendBlockingCommand(Command.Cmd code, String arg1, String arg2, String arg3) {
        System.out.println(String.format("Sending Command: %s", code));
        var blob = buildCommand(code, arg1, arg2, arg3);
        link.write(blob);
        var res = link.awaitBlob();
        return new Response(res.getCode(), res.getFieldList());
    }

    // Commands
    public Response authenticate(String user, String pass) {
        var ret = sendBlockingCommand(Command.Cmd.AUTH, user, pass, "");
        if (ret.Code)
            currentUser = user;
        return ret;
    }

    public Response newUser(String username, String email, String password) { return sendBlockingCommand(Command.Cmd.ADD, username, email, password); }

    public Response requestFriend(String user) { return sendBlockingCommand(Command.Cmd.REQUEST, user, "", ""); }

    public Response listFriends() { return sendBlockingCommand(Command.Cmd.LIST, "", "", ""); }

    public Response searchUsers(String username) { return sendBlockingCommand(Command.Cmd.SEARCH, username, "", ""); }

    public Response startChat(String username) { return sendBlockingCommand(Command.Cmd.CONV, username, "", ""); }

    public Message readMessage() {
        if (link.checkMessage()){
            var msg = link.getMessage();
            String username = "";
            if (!msg.getUsername().equals(currentUser))
                username = msg.getUsername();
            return new Message(msg.getTimestamp(), msg.getRoomid(), msg.getMessage(), username);
        } else
            return null;
    }

    public void sendMessage(String txt, int id) {
        var msg = ProtoMessages.Message.newBuilder();
        msg.setTimestamp((int)( System.currentTimeMillis() / 1000 ));
        msg.setRoomid(id);
        msg.setUsername(currentUser);
        msg.setMessage(txt);

        var blob = Blob.newBuilder();
        blob.setType(Blob.Type.Msg);
        blob.setMsg(msg);

        link.write(blob.build());
    }
}