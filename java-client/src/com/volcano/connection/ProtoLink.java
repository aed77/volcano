package com.volcano.connection;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class ProtoLink {
    private AsynchronousSocketChannel socket;
    private ConcurrentLinkedQueue<ProtoMessages.Response> responseQ;
    private ConcurrentLinkedQueue<ProtoMessages.Message> messageQ;
    private AtomicBoolean readPending;

    ProtoLink(String host, int ip) {
        responseQ = new ConcurrentLinkedQueue<>();
        messageQ = new ConcurrentLinkedQueue<>();
        readPending = new AtomicBoolean(false);

        try {
            socket = AsynchronousSocketChannel.open();
        } catch (IOException e) {
            System.err.println(String.format("Could not open socket: %s", e.getMessage()));
            System.exit(1);
        }

        socket.connect(new InetSocketAddress(host, ip), socket, new CompletionHandler<>() {
                @Override
                public void completed(Void aVoid, AsynchronousSocketChannel asynchronousSocketChannel) { System.out.println("Connection Success!"); }

                @Override
                public void failed(Throwable throwable, AsynchronousSocketChannel asynchronousSocketChannel) { System.err.println(String.format("Failed server connection: %s", throwable.getMessage())); }});

        var t = new Thread(() -> {
            try {
                Thread.sleep(250);
            } catch (Exception e) {} // This is fine
            while (true){
                if (!readPending.get()){
                    System.out.println("Calling Read");
                    this.read();
                    readPending.set(true);
                }
                try { Thread.sleep(10); } catch (Exception e) {} // No way this will happen
            }
        });
        t.start();
    }

    void write(ProtoMessages.Blob b) {
        var buffer = ByteBuffer.allocate(256);
        var stream = new ByteArrayOutputStream(256);

        try {
            b.writeTo(stream);
            buffer.put(stream.toByteArray());
            buffer.flip();
        } catch (IOException e) {
            System.err.println(String.format("Bad stream fill: %s", e.getMessage()));
            System.exit(1);
        }
        socket.write(buffer, socket, new CompletionHandler<>() {
            @Override
            public void completed(Integer integer, AsynchronousSocketChannel asynchronousSocketChannel) { System.out.println(String.format("Wrote: %d", integer)); }

            @Override
            public void failed(Throwable throwable, AsynchronousSocketChannel asynchronousSocketChannel) { System.err.println(String.format("Failed to write: %s", throwable.getMessage())); }
        });
    }

    private void read() {
        var buffer = ByteBuffer.allocate(256);
        readPending.set(true);

        socket.read(buffer, socket, new CompletionHandler<>() {
            @Override
            public void completed(Integer integer, AsynchronousSocketChannel asynchronousSocketChannel) {
                System.out.println(String.format("Read: %s", integer));
                readPending.set(false);

                try {
                    buffer.flip();
                    ProtoMessages.Blob blob = ProtoMessages.Blob.parseFrom(buffer);
                    System.out.println(String.format("Got Blob: %s", blob));
                    if (blob.hasRes())
                        responseQ.add(blob.getRes());
                    else if (blob.hasMsg())
                        messageQ.add(blob.getMsg());
                } catch (com.google.protobuf.InvalidProtocolBufferException e) {
                    System.err.println(String.format("Bad sendMessage: %s", e.getMessage()));
                    System.exit(1);
                }
            }

            @Override
            public void failed(Throwable throwable, AsynchronousSocketChannel asynchronousSocketChannel) { System.err.println(String.format("Read Failed: %s", throwable.getMessage())); }
        });
    }

    boolean checkMessage() {
        return (messageQ.size() != 0);
    }

    public ProtoMessages.Message getMessage() {
        return messageQ.remove();
    }

    ProtoMessages.Response awaitBlob() {
        while (responseQ.size() == 0)
            try {
                Thread.sleep(50);
            } catch (Exception e) {
                e.printStackTrace();
            }
        return responseQ.remove();
    }
}
