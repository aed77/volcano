package com.volcano.controllers;

import com.volcano.GrandController;
import com.volcano.connection.ServerConnection;
import com.volcano.views.LoginView;

import java.util.Map;
import java.util.function.Consumer;

public class LoginController extends BaseController {
    private LoginView view;
    public LoginController(LoginView view, Consumer<GrandController.View> pageChangeCallback){
        this.view = view;
        this.view.addCallbacks(Map.of(LoginView.Callbacks.LOGIN, map -> {
            var con = ServerConnection.getInstance();
            var res = con.authenticate((String)map.get("Username"), (String)map.get("Password"));
            if (res.Code) {
                System.out.println("Running Change Page");
                pageChangeCallback.accept(GrandController.View.MAIN);
            }
            else
                this.view.displayError("Login Failed");
        }, LoginView.Callbacks.REGISTER, map -> {
            pageChangeCallback.accept(GrandController.View.REGISTER);
        }));
    }
}
