package com.volcano.controllers;


import com.volcano.views.MainView;
import com.volcano.models.FriendMessageModel;

import java.util.Map;

public class MainViewController extends BaseController {
    public MainViewController(MainView view, FriendMessageModel model) {
        model.updateFriendList();
        view.addCallbacks(Map.of(
                MainView.Callbacks.SEARCH, map -> model.updateSearchResults((String)map.get("Field")),
                MainView.Callbacks.CHAT, map -> model.startChat((String)map.get("Field")),
                MainView.Callbacks.MESSAGE, map -> model.checkNewMessages(),
                MainView.Callbacks.OUT, map -> model.sendNewMessage((String)map.get("Field")),
                MainView.Callbacks.REQUEST, map -> model.requestFriend((String)map.get("Field"))
                ));
    }
}