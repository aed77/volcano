package com.volcano.controllers;

import com.volcano.GrandController.View;
import com.volcano.connection.ServerConnection;
import com.volcano.views.RegisterView;

import java.util.Map;
import java.util.function.Consumer;

public class RegisterController extends BaseController {
    private RegisterView view;
    public RegisterController(RegisterView view, Consumer<View> pageChangeCallback){
        this.view = view;
        this.view.addCallbacks(
                Map.of(
                        RegisterView.Callbacks.REGISTER, map -> {
                            var con = ServerConnection.getInstance();
                            var res = con.newUser((String)map.get("Username"), (String)map.get("Email"), (String)map.get("Password"));
                            if (res.Code) {
                                System.out.println("Running Change Page");
                                pageChangeCallback.accept(View.LOGIN);
                            }
                            else
                                this.view.displayError("Username/Email already exists");

                        },
                        RegisterView.Callbacks.BACK, map -> {
                            pageChangeCallback.accept(View.LOGIN);
                        }
                )
        );
    }
}
