//
// Created by Cameron Graybill on 5/27/18.
//

#ifndef SERVER_SERVER_HPP
#define SERVER_SERVER_HPP


#include <boost/asio.hpp>
#include "Logger.hpp"
#include "DatabaseConnector.hpp"
#include "CommandDispatcher.hpp"
#include "StateKeeper.hpp"

class Server {
public:
    Server(std::unique_ptr<Logger> &&lgr, StateKeeper & stateKeeper,
           std::unique_ptr<CommandDispatcher> &&cmds, boost::asio::io_service & loop, unsigned short port) : state(stateKeeper), logger{std::move(lgr)},
                                                        command_dispatcher{std::move(cmds)}, event_loop{loop}, connection_handler{loop, boost::asio::ip::tcp::endpoint{boost::asio::ip::tcp::v4(), port}} {}
    auto listen() -> void;
private:
    auto startAccept() -> void;
    std::unique_ptr<Logger> logger;
    std::unique_ptr<CommandDispatcher> command_dispatcher;
    StateKeeper & state;
    boost::asio::io_service & event_loop;
    boost::asio::ip::tcp::acceptor connection_handler;
};


#endif //SERVER_SERVER_HPP
