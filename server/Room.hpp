//
// Created by Cameron Graybill on 5/27/18.
//

#ifndef SERVER_ROOM_HPP
#define SERVER_ROOM_HPP


#include "Session.hpp"

class Room {
public:
    auto addUser(Session * user) -> void;
    auto removeUser(Session const & user) -> void;
    auto getUsers() const -> std::vector<Session *> const &;
private:
    std::vector<Session *> members;
};


#endif //SERVER_ROOM_HPP
