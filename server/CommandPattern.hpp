//
// Created by Cameron Graybill on 6/8/18.
//

#ifndef SERVER_COMMANDPATTERN_HPP
#define SERVER_COMMANDPATTERN_HPP

#include <volcano.pb.h>
#include "Logger.hpp"
#include "Session.hpp"

// If this wasn't obvious enough
struct CommandPattern {
    CommandPattern(Logger & lgr) : logger(lgr) {}
    virtual auto operator ()(Session & user, Response & response) -> void = 0;
    virtual ~CommandPattern() {}

protected:
    Logger & logger;
};


#endif //SERVER_COMMANDPATTERN_HPP
