//
// Created by Cameron Graybill on 5/27/18.
//

#ifndef SERVER_STATEKEEPER_HPP
#define SERVER_STATEKEEPER_HPP

#include <unordered_map>
#include <vector>
#include "Session.hpp"
#include "Room.hpp"

struct StateKeeper {
    StateKeeper(): last_room{0} {}
    auto createRoom() -> int;
    auto getRoom(int room_id) -> Room &;
    auto getRooms() -> std::unordered_map<int, Room>;
    auto logUserOut(Session & session) -> void;
    auto getSessions() -> std::vector<std::shared_ptr<Session>> const &;
    auto addUser(std::shared_ptr<Session> user) -> void;
    StateKeeper(StateKeeper const &) = delete;
    StateKeeper(StateKeeper &) = delete;

private:
    std::vector<std::shared_ptr<Session>> users;
    std::unordered_map<int, Room> rooms;
    int last_room;
};


#endif //SERVER_STATEKEEPER_HPP
