//
// Created by Cameron Graybill on 5/27/18.
//

#include "StateKeeper.hpp"

auto StateKeeper::createRoom() -> int {
    auto idx {last_room};
    rooms[idx] = Room();
    last_room++;
    return idx;
}

auto StateKeeper::logUserOut(Session & session) -> void {
    session.getSocket().close();
}

auto StateKeeper::getRoom(int room_id) -> Room & {
    return rooms[room_id];
}

auto StateKeeper::getRooms() -> std::unordered_map<int, Room> {
    return rooms;
}

auto StateKeeper::getSessions() -> std::vector<std::shared_ptr<Session>> const & {
    return users;
}

auto StateKeeper::addUser(std::shared_ptr<Session> user) -> void {
    users.push_back(user);
}
