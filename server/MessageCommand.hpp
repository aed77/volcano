//
// Created by Cameron Graybill on 6/8/18.
//

#ifndef SERVER_MESSAGECOMMAND_HPP
#define SERVER_MESSAGECOMMAND_HPP


#include "CommandPattern.hpp"
#include "StateKeeper.hpp"
#include "Parser.hpp"

struct MessageCommand : CommandPattern {
    MessageCommand(Logger &lgr, StateKeeper &skpr, Parser & p, std::string_view msg, uint32_t room_id, uint32_t times)
    : CommandPattern(lgr), sk(skpr), message(msg), roomid(room_id), timestamp(times), parser(p) {}

    void operator()(Session & user, Response & response) override {
        auto & room {sk.getRoom(roomid)};
        Blob b;
        Message *m = new Message();
        m->set_timestamp(timestamp);
        m->set_roomid(roomid);
        m->set_message(message);
        m->set_username(user.getUsername());
        b.set_allocated_msg(m);
        b.set_type(Blob_Type_Msg);
        std::array<char, 1024> buf{{}};
        auto len {parser.encodeInto(b, buf.data())};
        for (auto const & usr : room.getUsers())
            usr->sendData(buf.data(), len);
        response.set_code(true);
    }

private:
    StateKeeper & sk;
    std::string message;
    uint32_t roomid;
    uint32_t timestamp;
    Parser & parser;
};


#endif //SERVER_MESSAGECOMMAND_HPP
