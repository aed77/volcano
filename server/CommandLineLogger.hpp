//
// Created by Cameron Graybill on 5/27/18.
//

#ifndef SERVER_COMMANDLINELOGGER_HPP
#define SERVER_COMMANDLINELOGGER_HPP


#include "Logger.hpp"

class CommandLineLogger: public Logger {
public:
    void info(std::string_view msg) override;

    void warning(std::string_view msg) override;

    void error(std::string_view msg) override;

    void fatal(std::string_view msg) override;
};


#endif //SERVER_COMMANDLINELOGGER_HPP
