//
// Created by Cameron Graybill on 6/8/18.
//

#ifndef SERVER_SEARCHCOMMAND_HPP
#define SERVER_SEARCHCOMMAND_HPP

#include "CommandPattern.hpp"
#include "DatabaseConnector.hpp"
#include <volcano.pb.h>

struct SearchCommand : public CommandPattern {
    SearchCommand(Logger &lgr, DatabaseConnector &dbc, std::string_view username_)
            : CommandPattern(lgr), db(dbc), username(username_) {}

    void operator()(Session & user, Response & response) override {
        auto db_result {db.searchUserNames(user.getUsername(), username)};
        response.set_code(true);
        for (auto const & result: db_result) {
            response.add_field(result);
        }
    }

private:
    DatabaseConnector &db;
    std::string username;
};


#endif //SERVER_SEARCHCOMMAND_HPP
