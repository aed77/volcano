//
// Created by Cameron Graybill on 6/8/18.
//

#ifndef SERVER_LISTCOMMAND_HPP
#define SERVER_LISTCOMMAND_HPP


#include "CommandPattern.hpp"
#include "DatabaseConnector.hpp"

struct ListCommand : CommandPattern {
    ListCommand(Logger & lgr, DatabaseConnector & db): CommandPattern(lgr), dbc(db) {}

    void operator()(Session & user, Response & response) override {
        response.set_code(true);
        for (auto const & frnd : dbc.getUserFriends(user.getUsername())) {
            response.add_field(frnd);
        }
    }

private:
    DatabaseConnector & dbc;
};


#endif //SERVER_LISTCOMMAND_HPP
