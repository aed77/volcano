//
// Created by Cameron Graybill on 5/27/18.
//

#ifndef SERVER_LOGGER_HPP
#define SERVER_LOGGER_HPP


#include <string_view>

struct Logger {
    virtual auto info(std::string_view msg) -> void = 0;
    virtual auto warning(std::string_view msg) -> void = 0;
    virtual auto error(std::string_view msg) -> void = 0;
    virtual auto fatal(std::string_view msg) -> void = 0;
    virtual ~Logger() {}

};


#endif //SERVER_LOGGER_HPP
