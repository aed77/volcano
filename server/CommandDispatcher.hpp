//
// Created by Cameron Graybill on 5/27/18.
//

#ifndef SERVER_COMMANDS_HPP
#define SERVER_COMMANDS_HPP


#include <string_view>
#include "Session.hpp"
#include "Parser.hpp"
#include "StateKeeper.hpp"

class CommandDispatcher {
protected:
    virtual auto sendMessage(Session & user, uint32_t target_room, std::string_view message, uint32_t timestamp) const -> void = 0;
    virtual auto listUsers() const -> std::vector<std::string> = 0;
    virtual auto listFriends(Session & user) const -> std::vector<std::string> = 0;
public:
    // Return the number of bytes consumed
    CommandDispatcher(StateKeeper & sk, std::unique_ptr<Parser> && p, Logger & lgr): state(sk), parser(std::move(p)), logger(lgr) {}
    virtual auto actOnMessage(const char * data, std::size_t len, Session & user) const -> int = 0;
    virtual ~CommandDispatcher() {}

protected:
    std::unique_ptr<Parser> parser;
    Logger & logger;
    StateKeeper & state;
};


#endif //SERVER_COMMANDS_HPP
