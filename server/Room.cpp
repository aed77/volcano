//
// Created by Cameron Graybill on 5/27/18.
//

#include "Room.hpp"

auto Room::addUser(Session * user) -> void {
    members.push_back(user);
}

auto Room::removeUser(Session const & user) -> void {
    if (auto pos {std::find(members.begin(), members.end(), &user)}; pos != members.end())
        members.erase(pos);
}

auto Room::getUsers() const -> std::vector<Session *> const & {
    return members;
}


