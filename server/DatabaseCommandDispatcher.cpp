//
// Created by Cameron Graybill on 5/27/18.
//

#include "DatabaseCommandDispatcher.hpp"
#include "CommandPattern.hpp"
#include "AuthCommand.hpp"
#include "AddCommand.hpp"
#include "DelCommand.hpp"
#include "ListCommand.hpp"
#include "MessageCommand.hpp"
#include "RequestCommand.hpp"
#include "ConvCommand.hpp"
#include "SearchCommand.hpp"
#include "QuitCommand.hpp"

void DatabaseCommandDispatcher::sendMessage(Session &user, uint32_t target_room, std::string_view message, uint32_t timestamp) const {
    auto & room {state.getRoom(target_room)};
    Blob msg_blob;
    Message msg;
    msg.set_message(message.data());
    msg.set_roomid(target_room);
    msg.set_timestamp(timestamp);
    auto const serialized_message {msg_blob.SerializeAsString()};
    for (auto & iter_user : room.getUsers())
        iter_user->sendData(serialized_message.data(), serialized_message.length());
}

std::vector<std::string> DatabaseCommandDispatcher::listUsers() const {
    return database->getAllUsers();
}

std::vector<std::string> DatabaseCommandDispatcher::listFriends(Session &user) const {
    return database->getUserFriends(user.getUsername());
}

int DatabaseCommandDispatcher::actOnMessage(const char *data, std::size_t len,  Session &user) const {
    auto const input_blob {parser->parseFrom(data, len)};
    Blob output_blob;
    Response * response = new Response();
    std::array<char, 1024> buf {{}};
    std::unique_ptr<CommandPattern> command {nullptr};
    if (input_blob.has_cmd()){
        Command const & cmd = input_blob.cmd();
        switch (cmd.command()) {
            case Command_Cmd_AUTH:
                command = std::make_unique<AuthCommand>(logger, *database, cmd.arg1(), cmd.arg2());
                break;
            case Command_Cmd_ADD:
                command = std::make_unique<AddCommand>(logger, *database, cmd.arg1(), cmd.arg2(), cmd.arg3());
                break;
            case Command_Cmd_DEL:
                command = std::make_unique<DelCommand>(logger, *database, cmd.arg1());
                break;
            case Command_Cmd_LIST:
                command = std::make_unique<ListCommand>(logger, *database);
                break;
            case Command_Cmd_REQUEST:
                command = std::make_unique<RequestCommand>(logger, *database, cmd.arg1());
                break;
            case Command_Cmd_SEARCH:
                command = std::make_unique<SearchCommand>(logger, *database, cmd.arg1());
                break;
            case Command_Cmd_CONV:
                command = std::make_unique<ConvCommand>(logger, state, cmd.arg1());
                break;
            case Command_Cmd_QUIT:
                command = std::make_unique<QuitCommand>(logger, state);
                break;
            case Command_Cmd_Command_Cmd_INT_MAX_SENTINEL_DO_NOT_USE_:
                logger.warning("How did I even get this");
                break;
            case Command_Cmd_Command_Cmd_INT_MIN_SENTINEL_DO_NOT_USE_:
                logger.warning("How did I even get this");
                break;
        }
    } else if (input_blob.has_msg()) {
        Message const & msg = input_blob.msg();
        command = std::make_unique<MessageCommand>(logger, state, *parser.get(), msg.message(), msg.roomid(), msg.timestamp());
    } else {
        logger.warning("Got unexpected message " + input_blob.DebugString());
        return 0;
    }
    (*command)(user, *response);
    output_blob.set_allocated_res(response);
    output_blob.set_type(Blob_Type_Response);
    auto amount_to_send {parser->encodeInto(output_blob, buf.data())};
    logger.info("Sending: " + output_blob.DebugString());
    user.sendData(buf.data(), amount_to_send);
    return 0;
}
