//
// Created by Cameron Graybill on 5/27/18.
//

#include "SqliteDBC.hpp"

SqliteDBC *SqliteDBC::instance = 0;

SqliteDBC *SqliteDBC::getInstance() {
    if (instance == 0) {
        instance = new SqliteDBC();
    }
    return instance;
}

SqliteDBC::SqliteDBC() {}

auto SqliteDBC::setLogger(Logger &logger) -> void {
    dblogger = &logger;
}

auto SqliteDBC::getAllUsers() -> std::vector<std::string> {
    int rc;
    sqlite3_stmt *stmt;
    std::vector<std::string> allUsers;

    rc = sqlite3_prepare_v2(db, "select * from users;", -1, &stmt, NULL);
    if (rc != SQLITE_OK) {
        dblogger->error("Failed to select * from users\n");
        return allUsers;
    }

    while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
        std::string name = std::string(reinterpret_cast<const char *>(sqlite3_column_text(stmt, 0)));
        allUsers.push_back(name);
    }

    if (rc != SQLITE_DONE) {
        dblogger->warning("not done processing database for getAllUsers\n");
    }

    sqlite3_finalize(stmt);
    return allUsers;
}

auto SqliteDBC::searchUserNames(std::string_view self, std::string_view user) -> std::vector<std::string> {
    int rc;
    sqlite3_stmt *stmt;
    std::vector<std::string> someUsers;
    std::ostringstream oss;
    std::string sql;

    oss << "select F.name from ("
        << "  select name from users"
        << "  EXCEPT "
        << "  select first_user as name from user_relationships where second_user = \""
        << self
        << "\" and relationship_type = 0"
        << "  EXCEPT "
        << "select second_user as name from user_relationships where first_user = \""
        << self
        << "\" and relationship_type = 0"
        << ") F "
        << "where name != \""
        << self
        << "\" "
        << "and name like \"%"
        << user
        << "%\";";
    sql = oss.str();
    rc = sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
    if (rc != SQLITE_OK) {
        dblogger->error("Failed to search users\n");
        dblogger->error(sqlite3_errmsg(db));
        return someUsers;
    }

    while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
        std::string name = std::string(reinterpret_cast<const char *>(sqlite3_column_text(stmt, 0)));
        someUsers.push_back(name);
    }

    if (rc != SQLITE_DONE) {
        dblogger->warning("not done processing database for getAllUsers\n");
    }

    sqlite3_finalize(stmt);
    return someUsers;
}

auto SqliteDBC::getUserFriends(std::string_view user) -> std::vector<std::string> {
    int rc;
    sqlite3_stmt *stmt;
    std::ostringstream oss;
    std::string sql;
    std::vector<std::string> friends;

    oss << "select F.x from ("
        << " select first_user as x from user_relationships where second_user = \""
        << user << "\""
        << " and relationship_type = 0"
        << " union"
        << " select second_user as x from user_relationships where first_user = \""
        << user << "\""
        << " and relationship_type = 0"
        << ") F;";
    sql = oss.str();
    rc = sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
    if (rc != SQLITE_OK) {
        dblogger->error("Failed to getUserFriends\n");
    }

    while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
        friends.push_back(std::string(reinterpret_cast<const char *>(sqlite3_column_text(stmt, 0))));
    }

    if (rc != SQLITE_DONE) {
        dblogger->warning("not done processing database for getUserFriends\n");
    }

    sqlite3_finalize(stmt);
    return friends;
}

auto SqliteDBC::getUserPassword(std::string_view user) -> std::string {
    int rc;
    sqlite3_stmt *stmt;
    std::ostringstream oss;
    std::string sql;
    std::string password;

    oss << "select password from users where name is \"" << user << "\";";
    sql = oss.str();
    rc = sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
    if (rc != SQLITE_OK) {
        dblogger->error("Failed to getUserPassword\n");
    }

    while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
        password = std::string(reinterpret_cast<const char *>(sqlite3_column_text(stmt, 0)));
    }

    if (rc != SQLITE_DONE) {
        dblogger->warning("not done processing database for getUserPassword\n");
    }

    sqlite3_finalize(stmt);
    return password;
}

auto SqliteDBC::checkIfUserExists(std::string_view user) -> int {
    int rc;
    sqlite3_stmt *stmt;
    std::ostringstream oss;
    std::string sql;
    int count = 0;

    oss << "select name from users where name = \"" << user << "\";";
    sql = oss.str();

    rc = sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
    if (rc != SQLITE_OK) {
        dblogger->error("Failed to checkIfUserExists");
        dblogger->error(sqlite3_errmsg(db));
        //sqlite3_finalize(stmt);
        return -1;
    }
    while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
        count++;
    }

    if (rc != SQLITE_DONE) {
        dblogger->warning("not done processing database for checkIfUserExists\n");
    }

    sqlite3_finalize(stmt);
    return count;
}

auto SqliteDBC::registerUser(std::string_view user, std::string_view email, std::string_view password) -> int {
    int rc;
    sqlite3_stmt *stmt;
    std::ostringstream oss;
    std::string sql;

    if (checkIfUserExists(user) != 0) {
        dblogger->info("User already exists\n");
        return -1;
    }
    oss << "insert into users (name, email, password) values (\"" << user << "\", \"" << email << "\", \"" << password
        << "\");";
    sql = oss.str();

    rc = sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
    if (rc != SQLITE_OK) {
        dblogger->error("Failed to registerUser\n");
        sqlite3_finalize(stmt);
        return -1;
    }
    rc = sqlite3_step(stmt);
    if (rc != SQLITE_DONE) {
        dblogger->warning("not done processing database for registerUser\n");
    }

    sqlite3_finalize(stmt);
    return 0;
}

auto SqliteDBC::removeUserRelationship(std::string_view user_one, std::string_view user_two) -> int {
    int rc;
    sqlite3_stmt *stmt;
    std::ostringstream oss;
    std::string sql;

    // Sql check requires that first user < second user
    int comparison = user_one.compare(user_two);
    if (comparison > 0) {
        std::string_view temp = user_two;
        user_two = user_one;
        user_one = temp;
    }

    oss << "delete from user_relationships where first_user = \""
        << user_one
        << "\" and second_user = \""
        << user_two
        << "\";";
    sql = oss.str();

    rc = sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
    if (rc != SQLITE_OK) {
        dblogger->error("Failed to removeUserRelationship\n");
        sqlite3_finalize(stmt);
        return -1;
    }
    rc = sqlite3_step(stmt);
    if (rc != SQLITE_DONE) {
        dblogger->warning("not done processing database for removeUserRelationship\n");
    }

    sqlite3_finalize(stmt);
    return 0;
}

auto SqliteDBC::setRelationshipStatus(std::string_view user_one, std::string_view user_two,
                                      RelationshipStatus status) -> int {
    int rc;
    sqlite3_stmt *stmt;
    std::ostringstream oss;
    std::string sql;
    std::string_view requestor = user_one;
    int count = 0;

    // Sql check requires that first user < second user
    int comparison = user_one.compare(user_two);
    if (comparison > 0) {
        std::string_view temp = user_two;
        user_two = user_one;
        user_one = temp;
    }

    //does the relationship exist?
    oss << "select * from user_relationships where first_user = \""
        << user_one
        << "\" and second_user = \""
        << user_two
        << "\";";
    sql = oss.str();

    rc = sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
    if (rc != SQLITE_OK) {
        dblogger->error("Failed to check User Relationship\n");
        sqlite3_finalize(stmt);
        return -1;
    }
    while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
        count++;
    }
    if (rc != SQLITE_DONE) {
        dblogger->warning("not done processing database for check User Relationship\n");
    }
    sqlite3_finalize(stmt);

    sql = "";
    oss.str("");
    oss.clear();

    // 0 -> no relationship, just add one in
    // 1 -> there is a relationship, need to update it
    if (count == 0) {
        oss << "insert into user_relationships (first_user, second_user, requesting_user, relationship_type) values (\""
            << user_one
            << "\", \""
            << user_two
            << "\", \""
            << requestor
            << "\", \""
            << as_integer(status)
            << "\");";
        sql = oss.str();
    } else if (count > 0) {
        oss << "update user_relationships set relationship_type = \""
            << as_integer(status)
            << "\" where first_user = \""
            << user_one
            << "\" and second_user = \""
            << user_two
            << "\";";
        sql = oss.str();
    } else {
        dblogger->fatal("Something is seriously wrong with user relationships\n");
        return -1;
    }

    rc = sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
    if (rc != SQLITE_OK) {
        dblogger->error("Failed to set User Relationship\n");
        sqlite3_finalize(stmt);
        return -1;
    }
    rc = sqlite3_step(stmt);
    if (rc != SQLITE_DONE) {
        dblogger->warning("not done processing database for check User Relationship\n");
    }
    sqlite3_finalize(stmt);

    return 0;
}

auto SqliteDBC::getRelationshipStatus(std::string_view user_one, std::string_view user_two) -> RelationshipStatus {
    int rc;
    sqlite3_stmt *stmt;
    std::string rel_status;
    std::ostringstream oss;
    std::string sql;
    dblogger->info(user_one);
    dblogger->info(user_two);

    // Sql check requires that first user < second user
    int comparison = user_one.compare(user_two);
    if (comparison > 0) {
        std::string_view temp = user_two;
        user_two = user_one;
        user_one = temp;
    }

    oss << "select relationship_type from user_relationships where first_user = \""
        << user_one
        << "\" and second_user = \""
        << user_two
        << "\";";
    sql = oss.str();
    rc = sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
    if (rc != SQLITE_OK) {
        dblogger->error("Failed to get users relationship status\n");
        return RelationshipStatus::NONE;
    }
    rc = sqlite3_step(stmt);
    if (rc != SQLITE_ROW) {
        dblogger->error("Failed to get users relationship status\n");
    }
    auto x {sqlite3_column_text(stmt, 0)};
    if (!x) return RelationshipStatus::NONE;
    rel_status = std::string(reinterpret_cast<const char*>(x));
    rc = sqlite3_step(stmt);

    if (rc != SQLITE_DONE) {
        dblogger->warning("not done processing database for getAllUsers\n");
        dblogger->error(sqlite3_errmsg(db));
    }

    sqlite3_finalize(stmt);

    if (rel_status == "0") {
        dblogger->info("Friends");
        return RelationshipStatus::FRIENDS;
    } else if (rel_status == "1") {
        dblogger->info("Requested");
        return RelationshipStatus::REQUESTED;
    }
    dblogger->info("None");
    return RelationshipStatus::NONE;

}

auto SqliteDBC::openDB(std::string_view db_name) -> int {
    // Conversion to string since string_view doesn't guarantee null termination
    return sqlite3_open(std::string(db_name).c_str(), &db);
}

auto SqliteDBC::closeDB() -> int {
    return sqlite3_close(db);
}
