//
// Created by Cameron Graybill on 6/8/18.
//

#ifndef SERVER_QUITCOMMAND_HPP
#define SERVER_QUITCOMMAND_HPP

#include "CommandPattern.hpp"
#include "DatabaseConnector.hpp"
#include <volcano.pb.h>

struct QuitCommand : public CommandPattern {
    QuitCommand(Logger &lgr, StateKeeper &sk)
            : CommandPattern(lgr), statekeeper(sk) {}

    void operator()(Session & user, Response & response) override {
        statekeeper.logUserOut(user);
        response.set_code(true);
    }

private:
    std::string_view username;
    StateKeeper & statekeeper;
};


#endif //SERVER_QUITCOMMAND_HPP
