//
// Created by Cameron Graybill on 5/27/18.
//

#ifndef SERVER_DATABASECOMMANDDISPATCHER_HPP
#define SERVER_DATABASECOMMANDDISPATCHER_HPP


#include "CommandDispatcher.hpp"
#include "DatabaseConnector.hpp"

class DatabaseCommandDispatcher: public CommandDispatcher {
public:
    DatabaseCommandDispatcher(DatabaseConnector * dbc, StateKeeper & sk, std::unique_ptr<Parser> && p, Logger & lgr): CommandDispatcher(sk, std::move(p), lgr), database(dbc) {}

    int actOnMessage(const char *data, std::size_t len, Session &user) const override;

protected:

    void sendMessage(Session &user, uint32_t target_room, std::string_view message, uint32_t timestamp) const override;

    std::vector<std::string> listUsers() const override;

    std::vector<std::string> listFriends(Session &user) const override;

private:
    DatabaseConnector * database;
};


#endif //SERVER_DATABASECOMMANDDISPATCHER_HPP
