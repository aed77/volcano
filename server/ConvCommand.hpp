//
// Created by Cameron Graybill on 6/8/18.
//

#ifndef SERVER_CONVCOMMAND_HPP
#define SERVER_CONVCOMMAND_HPP

#include "CommandPattern.hpp"
#include "DatabaseConnector.hpp"
#include <volcano.pb.h>

struct ConvCommand : public CommandPattern {
    ConvCommand(Logger &lgr, StateKeeper &sk, std::string_view user_to_join)
            : CommandPattern(lgr), statekeeper(sk), other_user(user_to_join) {}

    void operator()(Session & user, Response & response) override {
        std::unordered_map<int, Room> rooms = statekeeper.getRooms();
        for(auto room: rooms)  {
            bool found_self = false;
            bool found_other = false;
            auto roomUsers {room.second.getUsers()};
            for(int i=0; i<roomUsers.size(); i++) {
                if(roomUsers.at(i)->getUsername() == user.getUsername()) {
                    found_self = true;
                }
                if(roomUsers.at(i)->getUsername() == other_user) {
                    found_other = true;
                }
            }
            if(found_self && found_other) {
                response.add_field(std::to_string(room.first));
                response.set_code(true);
                return;
            }
        }
        auto created {statekeeper.createRoom()};
        auto & room = statekeeper.getRoom(created);
        for (auto const x : statekeeper.getSessions()) {
            if (x->getUsername() == other_user)
                room.addUser(x.get());
        }
        room.addUser(&user);
        response.add_field(std::to_string(created));
        response.set_code(true);
    }

private:
    StateKeeper & statekeeper;
    std::string other_user;
};


#endif //SERVER_CONVCOMMAND_HPP
