//
// Created by Cameron Graybill on 6/8/18.
//

#ifndef SERVER_ADDCOMMAND_HPP
#define SERVER_ADDCOMMAND_HPP


#include "CommandPattern.hpp"
#include "DatabaseConnector.hpp"

struct AddCommand : CommandPattern {
    AddCommand(Logger & lgr, DatabaseConnector & db, std::string_view usr, std::string_view eml, std::string_view pas) : CommandPattern(lgr), dbc(db), username(usr), password(pas), email(eml) {}

    void operator()(Session & user, Response & response) override {
        auto rc {dbc.registerUser(username, email, password)};
        response.set_code(rc == 0);
    }

private:
    DatabaseConnector & dbc;
    std::string username;
    std::string password;
    std::string email;
};


#endif //SERVER_ADDCOMMAND_HPP
