//
// Created by Cameron Graybill on 5/27/18.
//

#ifndef SERVER_FILELOGGER_HPP
#define SERVER_FILELOGGER_HPP


#include "Logger.hpp"
#include <boost/filesystem.hpp>
#include <iostream>

class FileLogger : public Logger {
public:
    FileLogger(boost::filesystem::path path_to_file);

    void info(std::string_view msg) override;

    void warning(std::string_view msg) override;

    void error(std::string_view msg) override;

    void fatal(std::string_view msg) override;

    ~FileLogger() {output_file.close();}
private:
    std::ofstream output_file;
};


#endif //SERVER_FILELOGGER_HPP
