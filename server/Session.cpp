//
// Created by Cameron Graybill on 5/27/18.
//

#include "Session.hpp"
#include "CommandDispatcher.hpp"

auto Session::getSocket() -> boost::asio::ip::tcp::socket & {
    return sock;
}

auto Session::startRead() -> void {
    auto self(shared_from_this());
    sock.async_read_some(boost::asio::buffer(buffer, buf_size), [this, self] (const boost::system::error_code &err, std::size_t bytes_transferred) {
        if (!err) {
            this->command_dispatcher->actOnMessage(buffer.data(), bytes_transferred, *this);
            this->startRead();
        } else {
            logger->warning("User disconnected");
        }
    });
}

auto Session::getUsername() -> std::string {

    return username;
}

auto Session::setUsername(std::string name) -> void {
    username = name;
}

auto Session::sendData(const char *buf, std::size_t amount) -> void {
    logger->info("Sending " + std::to_string(amount) + " bytes");
    sock.write_some(boost::asio::buffer(buf, amount));
}
