//
// Created by Cameron Graybill on 5/27/18.
//

#include "ProtobufParser.hpp"

std::size_t ProtobufParser::encodeInto(Blob data, char *buf) {
    auto s {data.SerializeAsString()};
    data.SerializeToArray(buf, static_cast<int>(s.size()));
    return s.size();
}

Blob ProtobufParser::parseFrom(const char *buf, int length) {
    Blob b;
    b.ParseFromArray(buf, length);
    lgr.info("Got blob: " + b.DebugString());
    return b;
}
