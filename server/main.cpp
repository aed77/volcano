#include <iostream>
#include <boost/asio/io_service.hpp>
#include "Logger.hpp"
#include "DatabaseConnector.hpp"
#include "Server.hpp"
#include "CommandDispatcher.hpp"
#include "DatabaseCommandDispatcher.hpp"
#include "SqliteDBC.hpp"
#include "CommandLineLogger.hpp"
#include "ProtobufParser.hpp"

int main() {
    StateKeeper sk;
    std::unique_ptr<Logger> logger = std::make_unique<CommandLineLogger>();
    DatabaseConnector * db {SqliteDBC::getInstance()};
    SqliteDBC::getInstance()->setLogger(*logger.get());
    auto db_err {db->openDB("/home/neil/volcano/server/DB14")};
    if (db_err != SQLITE_OK)
        std::cout << "Got error while opening DB" << db_err << std::endl;
    std::unique_ptr<Parser> p = std::make_unique<ProtobufParser>(*logger.get());
    StateKeeper state;
    std::unique_ptr<CommandDispatcher> cmds = std::make_unique<DatabaseCommandDispatcher>(db, state, std::move(p), *logger.get());
    boost::asio::io_service loop;
    Server s{std::move(logger), state, std::move(cmds), loop, 8864};
    s.listen();
    loop.run();
    return 0;
}