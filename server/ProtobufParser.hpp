//
// Created by Cameron Graybill on 5/27/18.
//

#ifndef SERVER_PROTOBUFPARSER_HPP
#define SERVER_PROTOBUFPARSER_HPP


#include "Parser.hpp"

class ProtobufParser: public Parser {
public:
    ProtobufParser(Logger & lgr) : Parser(lgr) {}
    std::size_t encodeInto(Blob data, char *buf) override;

    Blob parseFrom(const char *buf, int length) override;
};


#endif //SERVER_PROTOBUFPARSER_HPP
