//
// Created by Cameron Graybill on 6/8/18.
//

#ifndef SERVER_AUTHCOMMAND_HPP
#define SERVER_AUTHCOMMAND_HPP

#include "CommandPattern.hpp"
#include "DatabaseConnector.hpp"
#include <volcano.pb.h>

struct AuthCommand : public CommandPattern {
    AuthCommand(Logger &lgr, DatabaseConnector &dbc, std::string_view username_, std::string_view password_)
            : CommandPattern(lgr), db(dbc), username(username_), password(password_) {}

    void operator()(Session & user, Response & response) override {
        auto db_pass {db.getUserPassword(username)};
        if (db_pass == password) {
            user.setUsername(username);
            response.set_code(true);
        } else {
            response.set_code(false);
        }
    }

private:
    DatabaseConnector &db;
    std::string username;
    std::string password;
};


#endif //SERVER_AUTHCOMMAND_HPP
