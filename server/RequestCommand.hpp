//
// Created by Cameron Graybill on 6/8/18.
//

#ifndef SERVER_REQUESTCOMMAND_HPP
#define SERVER_REQUESTCOMMAND_HPP

#include "CommandPattern.hpp"
#include "DatabaseConnector.hpp"
#include <volcano.pb.h>

struct RequestCommand : public CommandPattern {
    RequestCommand(Logger &lgr, DatabaseConnector &dbc, std::string_view username_)
            : CommandPattern(lgr), db(dbc), username(username_) {}

    void operator()(Session & user, Response & response) override {
        RelationshipStatus status {db.getRelationshipStatus(user.getUsername(), username) == RelationshipStatus::REQUESTED ? RelationshipStatus::FRIENDS : RelationshipStatus::REQUESTED};
        auto db_request {db.setRelationshipStatus(user.getUsername(), username, status)};
        response.set_code(db_request==0);
    }

private:
    DatabaseConnector &db;
    std::string username;
};


#endif //SERVER_REQUESTCOMMAND_HPP
