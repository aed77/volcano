//
// Created by Cameron Graybill on 6/8/18.
//

#ifndef SERVER_DELCOMMAND_HPP
#define SERVER_DELCOMMAND_HPP


#include "Logger.hpp"
#include "CommandPattern.hpp"
#include "DatabaseConnector.hpp"

struct DelCommand : CommandPattern {
    DelCommand(Logger & lgr, DatabaseConnector & db, std::string_view usr): CommandPattern(lgr),  dbc(db), user_to_remove(usr){}

    void operator()(Session & user, Response & response) override {
        response.set_code(dbc.removeUserRelationship(user.getUsername(), user_to_remove) == 0);
    }

private:
    DatabaseConnector & dbc;
    std::string user_to_remove;
};


#endif //SERVER_DELCOMMAND_HPP
