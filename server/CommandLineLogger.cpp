//
// Created by Cameron Graybill on 5/27/18.
//

#include "CommandLineLogger.hpp"
#include <iostream>

void CommandLineLogger::info(std::string_view msg) {
    std::cout << "INFO: " << msg << std::endl;
}

void CommandLineLogger::warning(std::string_view msg) {
    std::cout << "WARNING: " << msg << std::endl;

}

void CommandLineLogger::error(std::string_view msg) {
    std::cout << "ERROR: " << msg << std::endl;

}

void CommandLineLogger::fatal(std::string_view msg) {
    std::cout << "FATAL: " << msg << std::endl;

}
