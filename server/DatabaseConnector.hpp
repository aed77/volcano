//
// Created by Cameron Graybill on 5/27/18.
//

#ifndef SERVER_DATABASECONNECTOR_HPP
#define SERVER_DATABASECONNECTOR_HPP

#include <string_view>
#include <vector>
#include <string>
#include "Logger.hpp"

enum class RelationshipStatus {
    FRIENDS,
    REQUESTED,
    NONE
};

template <typename Enumeration>
auto as_integer(Enumeration const value)
    -> typename std::underlying_type<Enumeration>::type {
    return static_cast<typename std::underlying_type<Enumeration>::type>(value);
}

class DatabaseConnector {
    protected:
        Logger *dblogger;

    public:
        virtual auto setRelationshipStatus(std::string_view user_one, std::string_view user_two, RelationshipStatus status) -> int = 0;
        virtual auto getRelationshipStatus(std::string_view user_one, std::string_view user_two) -> RelationshipStatus = 0;
        virtual auto getAllUsers() -> std::vector<std::string> = 0;
        virtual auto searchUserNames(std::string_view self, std::string_view user) -> std::vector<std::string> = 0; // TODO: wildcard search *arg*
        virtual auto getUserFriends(std::string_view user) -> std::vector<std::string> = 0;
        virtual auto getUserPassword(std::string_view user) -> std::string = 0;
        virtual auto registerUser(std::string_view user, std::string_view email, std::string_view password) -> int = 0;
        virtual auto removeUserRelationship(std::string_view user_one, std::string_view user_two) -> int = 0;
        virtual auto openDB(std::string_view db_name) -> int = 0;
        virtual auto closeDB() -> int = 0;
        virtual ~DatabaseConnector() {}
};


#endif //SERVER_DATABASECONNECTOR_HPP
