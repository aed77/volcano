//
// Created by Cameron Graybill on 5/27/18.
//

#ifndef SERVER_PARSER_HPP
#define SERVER_PARSER_HPP
#include <volcano.pb.h>
#include "Logger.hpp"

class Parser {
public:
    Parser(Logger & lgr_) :lgr(lgr_) {}
    virtual auto encodeInto(Blob data, char * buf) -> std::size_t = 0;
    virtual auto parseFrom(const char * buf, int length) -> Blob = 0;
    virtual ~Parser() {};
protected:
    Logger & lgr;
};


#endif //SERVER_PARSER_HPP
