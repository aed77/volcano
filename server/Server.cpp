//
// Created by Cameron Graybill on 5/27/18.
//

#include "Server.hpp"
#include "Session.hpp"

auto Server::listen() -> void {
    startAccept();
}

auto Server::startAccept() -> void {
    // Create a socket to receive a request to
    auto s (std::make_shared<Session>(boost::asio::ip::tcp::socket(event_loop), command_dispatcher.get(), logger.get()));
    connection_handler.async_accept(s->getSocket(), [this, s] (boost::system::error_code const & error) {
        s->start();
        state.addUser(s);
        this->startAccept();
    });
}
