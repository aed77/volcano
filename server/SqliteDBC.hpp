//
// Created by Cameron Graybill on 5/27/18.
//

#ifndef SERVER_SQLITEDBC_HPP
#define SERVER_SQLITEDBC_HPP


#include "DatabaseConnector.hpp"
#include <iostream>
#include <sqlite3.h>
#include <vector>
#include <sstream>

class SqliteDBC : public DatabaseConnector {
    private:
      sqlite3 *db;
      static SqliteDBC* instance;

      auto checkIfUserExists(std::string_view user) -> int;
      SqliteDBC();

    public:
      static SqliteDBC* getInstance();
      auto setRelationshipStatus(std::string_view user_one, std::string_view user_two, RelationshipStatus status) -> int;
      auto getRelationshipStatus(std::string_view user_one, std::string_view user_two) -> RelationshipStatus;
      auto getAllUsers() -> std::vector<std::string>;
      auto searchUserNames(std::string_view self, std::string_view user) -> std::vector<std::string>;
      auto getUserFriends(std::string_view user) -> std::vector<std::string>;
      auto getUserPassword(std::string_view user) -> std::string;
      auto registerUser(std::string_view user, std::string_view email, std::string_view password) -> int;
      auto removeUserRelationship(std::string_view user_one, std::string_view user_two) -> int;
      auto openDB(std::string_view db_name) -> int;
      auto closeDB() -> int;
      auto setLogger(Logger &logger) -> void;

      SqliteDBC(SqliteDBC const&) = delete;
      void operator=(SqliteDBC const&)  = delete;

};


#endif //SERVER_SQLITEDBC_HPP
