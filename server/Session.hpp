//
// Created by Cameron Graybill on 5/27/18.
//

#ifndef SERVER_SESSION_HPP
#define SERVER_SESSION_HPP
#include <boost/asio.hpp>
#include "Logger.hpp"

class CommandDispatcher;

class Session : public std::enable_shared_from_this<Session> {
public:
    Session(boost::asio::ip::tcp::socket && incoming_socket, CommandDispatcher const * const dispatcher, Logger * lgr):
            sock(std::move(incoming_socket)), username(""), command_dispatcher(dispatcher), logger(lgr) {}
    auto start() -> void {startRead();}
    auto getSocket() -> boost::asio::ip::tcp::socket &;
    auto getUsername() -> std::string;
    auto setUsername(std::string name) -> void;
    auto sendData(const char * buf, std::size_t amount) -> void;
private:
    auto startRead() -> void;
    boost::asio::ip::tcp::socket sock;
    std::string username;
    CommandDispatcher const * const command_dispatcher;
    static constexpr int buf_size {1024};
    std::array<char, buf_size> buffer {{}};
    Logger * logger;
};


#endif //SERVER_SESSION_HPP
