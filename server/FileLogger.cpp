//
// Created by Cameron Graybill on 5/27/18.
//

#include "FileLogger.hpp"
#include <iostream>

void FileLogger::info(std::string_view msg) {
    output_file << "INFO: " << msg << std::endl;
}

void FileLogger::warning(std::string_view msg) {
    output_file << "WARNING: " << msg << std::endl;
}

void FileLogger::error(std::string_view msg) {
    output_file << "ERROR: " << msg << std::endl;
}

void FileLogger::fatal(std::string_view msg) {
    output_file << "FATAL: " << msg << std::endl;
}

FileLogger::FileLogger(boost::filesystem::path path_to_file) {
    output_file.open(path_to_file.string());
}

